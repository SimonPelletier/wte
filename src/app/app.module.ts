import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';

import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';


import { StarRatingModule } from 'angular-star-rating';

import "materialize-css";
import { MaterializeModule } from "angular2-materialize";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { RestaurantsService } from './restaurants.service';

import { GeocoderService } from './geocoder.service';
import { PlacesService } from './places.service';

import { AppComponent } from './app.component';
import { ContentComponent } from './content/content.component';
import { MapComponent } from './map/map.component';
import { RestaurantsListComponent } from './restaurants-list/restaurants-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    MapComponent,
    RestaurantsListComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    StarRatingModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCY2YSw8RU-njyTNebCy0-FlH33LleOt-4',
      libraries: ["places"]
    })
  ],
  providers: [
    RestaurantsService,
    GeocoderService,
    PlacesService,
    GoogleMapsAPIWrapper
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
