import { Injectable, NgZone } from '@angular/core';
import { AgmCoreModule, GoogleMapsAPIWrapper, MapsAPILoader } from '@agm/core';
import { Observable, Observer } from 'rxjs';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class GeocoderService extends GoogleMapsAPIWrapper {

  currentAddress:string = 'Adresse non trouvée';

  constructor(
    private __loader :MapsAPILoader,
    private __zone :NgZone
  ) {
    super(__loader, __zone);
  }

  getAddress(lat, lng){
    return Observable.create(observer => {
      try {
        this.__loader.load().then(() => {

          let geocoder = new google.maps.Geocoder();
          let latlng = new google.maps.LatLng(lat, lng);

          geocoder.geocode({'latLng': latlng}, (results, status) => {

            if (status === google.maps.GeocoderStatus.OK) {
              const place = results[0].formatted_address;
              observer.next(place);
              observer.complete();
            } else {
              console.error('Error - ', results, ' & Status - ', status);
              if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                observer.error('Address not found!');
              }else {
                observer.error(status);
              }
              observer.complete();
            }

          });
        });
      } catch (error) {
        observer.error('error getGeocoding' + error);
        observer.complete();
      }
    });
  }

}
