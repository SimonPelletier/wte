import { Component, OnInit, ViewEncapsulation, NgZone } from '@angular/core';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';

import { GeocoderService } from '../geocoder.service';
import { PlacesService } from '../places.service';


import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class MapComponent implements OnInit {

  restaurants: Restaurant[] = null;

  zoom: number = 7;

  lat: number = 51.678418;
  lng: number = 7.809007;

  map: any;
  center: any;

  locationFound = false;

  closeResult: string;

  newRestaurantName: string;
  newAddress: any = 'adresse temporaire';

  newLat: number;
  newLng: number;



  public iconUrl = 'assets/images/marker.svg';


  constructor(
    private _restaurantsService: RestaurantsService,
    private _modalService: NgbModal,
    private _geocoderService: GeocoderService,
    private _placesService: PlacesService
  ) { }

  ngOnInit() {
    this.findMe();
    this.getRestaurants();
  }

  openModal(content) {
    this._modalService.open(content, { size: 'lg' });
  }

  getRestaurants(): void {
    this.restaurants = this._restaurantsService.getRestaurants();
  }

  boundsChange(latLngBounds) {
    this._restaurantsService.neLat = latLngBounds.getNorthEast().lat();
    this._restaurantsService.neLng = latLngBounds.getNorthEast().lng();
    this._restaurantsService.swLat = latLngBounds.getSouthWest().lat();
    this._restaurantsService.swLng = latLngBounds.getSouthWest().lng();
  }

  private addRestaurant(event, content){
    this._geocoderService.getAddress(event.coords.lat, event.coords.lng)
      .subscribe(
        result => {
          this.newAddress = result;
          this.newLat = event.coords.lat;
          this.newLng = event.coords.lng;
          this.openModal(content);
        },
        error => console.log(error)
      );
  } 

  private confirmRestaurant(){
    this._restaurantsService.addRestaurant(this.newRestaurantName, this.newAddress, this.newLat, this.newLng);
    this.newRestaurantName = '';

  }

  private findMe(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        this.locationFound = true;
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  private zoomChange(e){
    this.zoom = e;
  }

  private centerChange(e){
    this.center = e;
  }

  mapReady(e){
    this.map = e;
  }

  idleMap(){
    var results;
    if (this.zoom >= 10) {
      results = this._placesService.addCloseRestaurant(this.center, this.map);
    }
  }

}
