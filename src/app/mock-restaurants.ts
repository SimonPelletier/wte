import { Restaurant } from './restaurant';

export const RESTAURANTS: Restaurant[] = [
   {
      "restaurantName":"Hôtel Le Père Léon",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":42.702816,
      "lng":2.879504,
      "ratings":[
         {
            "stars":3.5,
            "comment":"Cadre agréable, le concept est très sympa d'autant plus que sur Muret on ne peut trouver un lieu semblable. Propose des soirées à thème : jeux, concert, spécialité régionale......."
         },
         {
            "stars":2,
            "comment":"Établissement froid, serveurses très accueillante et souriante ! Patron froid menu normal. Attention, parking surveiller de près !"
         }
      ]
   },
   {
      "restaurantName":"Le Saint Sauvage",
      "address":"4 Rue Lamarck, 75018 Paris",
      "lat":43.184039,
      "lng":3.002927,
      "ratings":[
         {
            "stars":4,
            "comment":"Intérieur sympa par rapport aux autres restos alentour. Nourriture passable. Un peu cher pour ce que c'est."
         },
         {
            "stars":3,
            "comment":"Bonne table et cuisine traditionnelle très bonne."
         }
      ]
   },
   {
      "restaurantName":"Le Matin",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":43.338175,
      "lng":3.226575,
      "ratings":[
         {
            "stars":4,
            "comment":"Un excellent restaurant, j'y reviendrai ! Par contre il vaut mieux aimer la viande."
         },
         {
            "stars":5,
            "comment":"Tout simplement mon restaurant préféré !"
         }
      ]
   },
   {
      "restaurantName":"Restaurant Emile",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":43.410246,
      "lng":3.671763,
      "ratings":[
         {
            "stars":4,
            "comment":"Rapide, bien."
         },
         {
            "stars":5,
            "comment":"Excellent comme d'habitude. De la fraîcheur pour les produits, une équipe sympa et des sourires."
         },
         {
            "stars":5,
            "comment":"Vraiment parfait . De la très bonne cuisine et une super ambiance . Je recommande direct"
         },
         {
            "stars":5,
            "comment":"Tout simplement mon restaurant préféré !"
         }
      ]
   },
   {
      "restaurantName":"La Braisière",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":43.596716,
      "lng":3.885772,
      "ratings":[
         {
            "stars":4,
            "comment":"Des produits frais, des saveurs originales, un accueil chaleureux pour un prix très correct"
         },
         {
            "stars":4,
            "comment":"Des prix au top pour des portions généreuses. J'y suis allée plusieurs fois, et la nourriture ne m'a jamais déçue. Bonne trouvaille"
         }
      ]
   },
   {
      "restaurantName":"Pasta Pizza",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":43.820182,
      "lng":4.353331,
      "ratings":[
         {
            "stars":1,
            "comment":"En effet les pizzas sont à un prix correct mais ne vous attendez pas à une qualité extraordinaire. Beaucoup trop d'huile suintant de la pizza à mon goût."
         },
         {
            "stars":5,
            "comment":"Excellentes pizzas ! Livraison top, rien à dire, je recommande !!"
         },
         {
            "stars":4,
            "comment":"Très sympa"
         },
         {
            "stars":0,
            "comment":"Pizzas et kebab beaucoup trop salées, cependant le prix reste correct ."
         }
      ]
   },
   {
      "restaurantName":"Borriquito Loco",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":43.940005,
      "lng":4.810165,
      "ratings":[
         {
            "stars":5,
            "comment":"Excellente pierrade et un patron super sympathique."
         },
         {
            "stars":4,
            "comment":"Un lieu ou tout est bon et le patron est devenu un ami a essayé."
         }
      ]
   },
   {
      "restaurantName":"Killarney",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":44.143230,
      "lng":4.809836,
      "ratings":[
         {
            "stars":2,
            "comment":"Pour 10 € c'est un peu frugal un dessert dans le menu ne serai pas de trop."
         },
         {
            "stars":0,
            "comment":"Waps froid à peine cuit toute la nuit malade ... sur 4 on est 2 a être malade je trouve pas sa correct . Je déconseille tracer votre chemin"
         },
         {
            "stars":4,
            "comment":"Rapport qualité prix très correct et bonne nourriture."
         },
         {
            "stars":4,
            "comment":"Terrasse agréable et patron sympathique. Menu correct."
         }
      ]
   },
   {
      "restaurantName":"La Table de William",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":42.566507,
      "lng":2.865732,
      "ratings":[
         {
            "stars":5,
            "comment":"Accueil super sympa, de bons conseils sur le vin. Présentation et explication du menu qui donne envie. La viande était très bonne. Dans un cadre sympa et calme. Je recommande vivement. Merci pour le repas."
         },
         {
            "stars":3,
            "comment":"Accueil agréable au premier abord. Décoration sympa. Mais plusieurs plats à la carte non disponibles. Attente assez longue alors qu'il n'y avait pas grand monde dans le resto. Pas les meilleurs burgers que j'ai mangé..."
         },
         {
            "stars":5,
            "comment":"Un excellent restaurant, j'y reviendrai ! Par contre il vaut mieux aimer la viande."
         },
         {
            "stars":5,
            "comment":"Tout simplement mon restaurant préféré !"
         }
      ]
   },
   {
      "restaurantName":"Les Avions",
      "address":"39 Rue des Petites Écuries, 75010 Paris",
      "lat":42.522419,
      "lng":2.829946,
      "ratings":[
         {
            "stars":4,
            "comment":"Excellente cuisine, personnel très sympa et prix raisonnable."
         },
         {
            "stars":5,
            "comment":"Succulent ! Les plats sont très bons, l'ambiance au top tout comme le service ! Je le recommande fortement !"
         },
         {
            "stars":5,
            "comment":"Tout simplement mon restaurant préféré !"
         }
      ]
   }
];
