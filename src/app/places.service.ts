import { Injectable, NgZone } from '@angular/core';
import { AgmCoreModule, GoogleMapsAPIWrapper, MapsAPILoader } from '@agm/core';
import { Rating } from './rating';

import { RestaurantsService } from './restaurants.service';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  foundRestaurants:Array<any> = [];

  constructor(
    private _restaurantsService: RestaurantsService,
    private _map: GoogleMapsAPIWrapper
  ) {}

  isAlreadyOnMap(id){
    if (this.foundRestaurants){
      var exist;
      for (var i = 0; i < this.foundRestaurants.length; i++){
        if (this.foundRestaurants[i] == id){
          exist = true;
          break;
        } else {
          exist = false;
        }
      }
      return exist;
    } else {
      return false;
    }
  }

  addCloseRestaurant(centerOfMap, map){

    var service = new google.maps.places.PlacesService(map);
    let request = { location : centerOfMap, radius : '1000', types: ["restaurant"] };
    var self = this;

    service.nearbySearch(request,function(results,status){
        if(status === google.maps.places.PlacesServiceStatus.OK){

          for (var i = 0; i < results.length; i++){
            var placeID = {placeId: results[i].place_id};
            service.getDetails(placeID, function(result, status){
              if (status === google.maps.places.PlacesServiceStatus.OK){
                if (self.isAlreadyOnMap(result.place_id)){
                } else {
                  self.foundRestaurants.push(result.place_id);
                  var name = result.name;
                  var address = (result.formatted_address).slice(0, -8);;
                  var lat = result.geometry.location.lat();
                  var lng = result.geometry.location.lng();
                  var ratings = [];
                  if (result.reviews){
                    for (var j = 0; j < result.reviews.length; j++){
                      var aRating = new Rating;
                      aRating.stars = result.reviews[j].rating;
                      aRating.comment = result.reviews[j].text;
                      ratings.push(aRating);
                    }
                    self._restaurantsService.addRestaurantWithSearch(name, address, lat, lng, ratings);
                  }
                }
              }
            });
          }
        }
    });
  }

}
