export class Rating {
  stars: number;
  comment: string;
}
