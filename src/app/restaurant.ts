export class Restaurant {
  restaurantName: string;
  address: string;
  lat: number;
  lng: number;
  ratings: any;
}
