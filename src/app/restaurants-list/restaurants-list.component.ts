import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../restaurant';
import { RestaurantsService } from '../restaurants.service';

@Component({
  selector: 'app-restaurants-list',
  templateUrl: './restaurants-list.component.html',
  styleUrls: ['./restaurants-list.component.scss'],
  providers: []
})
export class RestaurantsListComponent implements OnInit {

  restaurants: Restaurant[] = null;
  stars: number = 2.5;
  comment: string = '';

  constructor(private _restaurantsService: RestaurantsService) { }

  ngOnInit(): void {
    this.getRestaurants();
  }

  getRestaurants(): void {
    this.restaurants = this._restaurantsService.getRestaurants();
  }

  getRestaurantImage(id){
    var restaurantLocation = "location=" + this.restaurants[id].lat + "," + this.restaurants[id].lng;
    var streetViewImg = '//maps.googleapis.com/maps/api/streetview?size=100x100&key=AIzaSyCY2YSw8RU-njyTNebCy0-FlH33LleOt-4&' + restaurantLocation;
    return streetViewImg;
  }

  getAverage(id){
    var sum = 0;
    for (var i =0; i < this.restaurants[id].ratings.length; i++){
      sum = sum + this.restaurants[id].ratings[i].stars;
    }
    sum = sum / this.restaurants[id].ratings.length;
    return sum;
  }

  addRating(id){
    if (this.comment != ''){
      this._restaurantsService.addRating(id, this.stars, this.comment);
      this.comment = '';
      this.stars = 2.5;
    }
  }

  starClickChange(e){
    this.stars = e.rating;
  }


}
