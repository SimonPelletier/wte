import { Injectable, NgZone } from '@angular/core';
import { Restaurant } from './restaurant';
import { Rating } from './rating';
import { RESTAURANTS } from './mock-restaurants';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  private localRestaurants:Restaurant[] = [];

  neLat:number;
  neLng:number;
  swLat:number;
  swLng:number;

  zoom:number;

  rateMin:number = 0;
  rateMax:number = 5;

  constructor() {
    this.localRestaurants = RESTAURANTS;
  }

  getRestaurants(): Restaurant[] {
    return this.localRestaurants;
  }

  addRating(id, stars, comment){
    var aRate = new Rating;
    aRate.stars = stars;
    aRate.comment = comment;

    this.localRestaurants[id].ratings.push(aRate)
  }

  addRestaurant(name, address, lat, lng){
    var aRestaurant = new Restaurant;
    aRestaurant.restaurantName = name;
    aRestaurant.address = address;
    aRestaurant.lat = lat;
    aRestaurant.lng = lng;
    aRestaurant.ratings = [];

    this.localRestaurants.push(aRestaurant);
  }

  addRestaurantWithSearch(name, address, lat, lng, ratings){
    var aRestaurant = new Restaurant;
    aRestaurant.restaurantName = name;
    aRestaurant.address = address;
    aRestaurant.lat = lat;
    aRestaurant.lng = lng;
    aRestaurant.ratings = ratings;

    this.localRestaurants.push(aRestaurant);
  }

  setCoords(neLat, neLng, swLat, swLng){
    this.neLat = neLat;
    this.neLng = neLng;
    this.swLat = swLat;
    this.swLng = swLng;
  }

  getCoords(){
    return [
      this.neLat,
      this.neLng,
      this.swLat,
      this.swLng
    ]
  }

  isVisible(i){
    if (
      this.localRestaurants[i].lat < this.neLat
      && this.localRestaurants[i].lat > this.swLat
      && this.localRestaurants[i].lng < this.neLng
      && this.localRestaurants[i].lng > this.swLng
    ){
      return true;
    } else {
      return false;
    }
  }

  filteredRestaurant(i){
    var average = 0;
    if (this.localRestaurants[i].ratings.length == 0){

    } else {
      var sum = 0;
      for (var j = 0; j < this.localRestaurants[i].ratings.length; j++){
        sum = sum + this.localRestaurants[i].ratings[j].stars;
      }
      average = sum / this.localRestaurants[i].ratings.length;
    }

    if (average >= this.rateMin && average <= this.rateMax) {
      return true;
    } else {
      return false;
    }

  }

  minChange(e){
    if (e.rating <= this.rateMax){
      this.rateMin = e.rating;
    } else {
      this.rateMax = e.rating;
      this.rateMin = e.rating;
    }
  }

  maxChange(e){
    if (e.rating >= this.rateMin){
      this.rateMax = e.rating;
    } else {
      this.rateMax = e.rating;
      this.rateMin = e.rating;
    }
  }

}
